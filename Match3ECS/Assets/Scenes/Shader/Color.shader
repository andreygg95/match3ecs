Shader "Calligraphy/Color" {
	
	Properties {
	_Color ("Color", Color) = (1,1,1,1)
	}
	
	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200	
		Lighting Off	
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		Fog { Mode Off }	
		
		Pass
		{			
			CGPROGRAM
			#pragma vertex vertexColor
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			
			struct vertex_input { float4 vertex : POSITION; };
			
			struct vertex_output { float4 vertex : SV_POSITION; };	
			
			vertex_output vertexColor(vertex_input v) { vertex_output o; o.vertex = UnityObjectToClipPos(v.vertex); return o; }

			fixed4 _Color;	

			half4 fragmentColor(vertex_output i) : COLOR { return _Color; }				
			ENDCG
        }
	}
}
