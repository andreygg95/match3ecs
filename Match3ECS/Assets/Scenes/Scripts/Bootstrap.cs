﻿using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    private Feature _globalFeature;
    private void Awake() => _globalFeature = new GameFeature(Contexts.sharedInstance.game);

    private void Start() => _globalFeature.Initialize();
    
    private void Update() => _globalFeature.Execute();
    
    private void OnDestroy() => _globalFeature.TearDown();
}
