﻿using UnityEngine;

public class TapHandler : MonoBehaviour
{
    private static int tapCount;
    private void OnMouseUpAsButton()
    {
        var tap = Contexts.sharedInstance.game.CreateEntity();
        tap.AddTap(gameObject, tapCount++);
    }
}
