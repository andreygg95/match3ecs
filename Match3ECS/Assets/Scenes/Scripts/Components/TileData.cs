﻿using UnityEngine.Analytics;

public class TileData
{
    public enum TileColor
    {
        Empty,
        Red,
        Blue,
        Green,
        Yellow,
        Pink,
        Orange
    }

    public const int MaxColorInt = 7;
}