﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

public class Tile : IComponent { }

public class GravityTile : IComponent { }

public class CreateGravityAfterDestroy : IComponent { }

public class TileColor : IComponent
{
    public TileData.TileColor value;
}

public class PositionX : IComponent
{
    [EntityIndex]
    public int value;
}

public class PositionY : IComponent
{
    [EntityIndex]
    public int value;
}

public class WorldPosition : IComponent
{
    public Vector3 value;
}

public class Moving : IComponent { }

public class Hide : IComponent { }

public class StartedDissapear : IComponent { }

public class Collected : IComponent { }

public class Destroy : IComponent { }

public class Scale : IComponent { }

public class MoveSpeed : IComponent
{
    public float value;
}

[Unique]
public class GravityDirection : IComponent
{
    public Vector2 value;
}

public class ChangeGravity : IComponent { }

[Unique]
public class GravityValue : IComponent
{
    public float value;
}

[Unique]
public class FieldData : IComponent
{
    public int xSize;
    public int ySize;
}

public class GameObjectLink : IComponent
{
    [PrimaryEntityIndex]
    public GameObject value;
}

public class ColliderEnabled : IComponent { }
public class Collider : IComponent { public BoxCollider value; }

public class Tap : IComponent
{
    public GameObject target;
    public int tapCount;
}