﻿using Entitas;
using UnityEngine;

public class HideSystem : IExecuteSystem
{
    private IGroup<GameEntity> collected;
    public HideSystem(GameContext gameContext)
    {
        collected = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tile, GameMatcher.Hide));
    }
    
    public void Execute()
    {
        var dettaScale = Time.deltaTime * 2f;

        foreach (var entity in collected.GetEntities())
        {
            entity.isStartedDissapear = true;
            var trfm = entity.gameObjectLink.value.transform;
            var scale = trfm.localScale.x - dettaScale;
            trfm.localScale = Vector3.one * scale;
            if (scale < 0)
                Collect(entity);
        }
    }

    private void Collect(GameEntity entity)
    {
        entity.isCollected = true;
    }
}