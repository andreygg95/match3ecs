﻿using Entitas;
using UnityEngine;

public class ColliderSystem : IExecuteSystem
{
    private IGroup<GameEntity> entities;
    
    public ColliderSystem(GameContext game)
    {
        entities = game.GetGroup(GameMatcher.AllOf(GameMatcher.Tile, GameMatcher.Collider));
    }

    public void Execute()
    {
        foreach (GameEntity entity in entities.GetEntities())
        {
            var colliderEnabled = !entity.isMoving && !entity.isHide;
            if (colliderEnabled != entity.isColliderEnabled)
            {
                entity.isColliderEnabled = colliderEnabled;
                entity.collider.value.enabled = colliderEnabled;
            }
        }
    }
}