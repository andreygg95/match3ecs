﻿using Entitas;
using UnityEngine;

public class TapSystem : IExecuteSystem
{
    private GameContext game;
    private IGroup<GameEntity> taps;
    public TapSystem(GameContext gameContext)
    {
        game = gameContext;
        taps = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tap));
    }
    
    public void Execute()
    {
        var arr = taps.GetEntities();
        if (arr.Length == 1)
            SetScale(game.GetEntityWithGameObjectLink(arr[0].tap.target),1.5f);

        if (arr.Length == 2)
        {
            GameEntity prevTap, lastTap;
            int lastIndex = arr[0].tap.tapCount > arr[1].tap.tapCount ? 0 : 1;
            prevTap = arr[1 - lastIndex];
            lastTap = arr[lastIndex];
            var prevEntity = game.GetEntityWithGameObjectLink(prevTap.tap.target);
            var lastEntity = game.GetEntityWithGameObjectLink(lastTap.tap.target);
            var distance = Mathf.Abs(prevEntity.positionX.value - lastEntity.positionX.value) +
                           Mathf.Abs(prevEntity.positionY.value - lastEntity.positionY.value);

            prevTap.Destroy();
            SetScale(prevEntity, 1);
            
            if (distance == 0)
            {
                lastTap.Destroy();
                return;
            }
            
            if (distance == 1)
            {
                lastTap.Destroy();
                ChangePositions(prevEntity, lastEntity);
                return;
            }
            
            SetScale(lastEntity, 1.5f);
        }
    }

    private void ChangePositions(GameEntity e1, GameEntity e2)
    {
        var positionX1 = e1.positionX.value;
        var positionY1 = e1.positionY.value;
        var worldPosition = e1.worldPosition.value;

        e1.ReplacePositionX(e2.positionX.value);
        e1.ReplacePositionY(e2.positionY.value);
        e1.ReplaceWorldPosition(e2.worldPosition.value);

        e2.ReplacePositionX(positionX1);
        e2.ReplacePositionY(positionY1);
        e2.ReplaceWorldPosition(worldPosition);
    }

    private void SetScale(GameEntity entity, float scale)
    {
        entity.gameObjectLink.value.transform.localScale = Vector3.one * scale;
    }
}