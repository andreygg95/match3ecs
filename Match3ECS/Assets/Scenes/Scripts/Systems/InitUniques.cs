﻿using Entitas;
using UnityEngine;

public class InitUniques : IInitializeSystem
{
    private GameContext game;
    public InitUniques(GameContext game)
    {
        this.game = game;
    }

    public void Initialize()
    {
        var data = game.CreateEntity();
        data.AddFieldData(12,10);
        data.AddGravityDirection(Vector2.up);
        data.AddGravityValue(9.8f);
    }
}