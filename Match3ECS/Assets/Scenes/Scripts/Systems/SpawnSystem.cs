﻿using System.Linq;
using Entitas;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnSystem : IExecuteSystem
{
    private GameContext game;

    public SpawnSystem(GameContext gameContext)
    {
        game = gameContext;
    }

    public void Execute()
    {
        int x = game.fieldData.xSize;
        int y = game.fieldData.ySize;
        bool moveDown = game.gravityDirection.value.y < 0;
        for (int i = 0; i < x; i++)
        {
            var column = game.GetEntitiesWithPositionX(i);
            if (column.Count < y)
            {
                int yPos = column.Count > 0
                    ? Round(moveDown
                        ? Mathf.Max(y,column.Max(item => item.worldPosition.value.y) + 1)
                        : Mathf.Min(-1,column.Min(item => item.worldPosition.value.y) - 1))
                    : moveDown
                        ? y
                        : -1;

                if (moveDown)
                    for (int j = column.Count; j < y; j++)
                        Spawn(i, j, yPos++);
                else
                    for (int j = y - column.Count - 1; j >= 0; j--)
                        Spawn(i, j, yPos--);
            }
        }
    }

    private int Round(float value)
    {
        if (Mathf.Approximately(value, (int) value)) return (int) value;
        return (int) value + 1;
    }

    private GameEntity Spawn(int x, int y, int yWorldPos)
    {
        var entity = game.CreateEntity();
        entity.isTile = true;
        entity.AddTileColor(RandomColor());
        entity.AddPositionX(x);
        entity.AddPositionY(y);
        entity.AddWorldPosition(new Vector3(x, yWorldPos));
        entity.isMoving = true;
        return entity;
    }

    private TileData.TileColor RandomColor() => (TileData.TileColor) Random.Range(1, TileData.MaxColorInt);
}