﻿using System.Linq;
using Entitas;
using UnityEngine;

public class CombinationsFeature : IExecuteSystem
{
    private GameContext game;
    public CombinationsFeature(GameContext gameContext)
    {
        game = gameContext;
    }
    
    public void Execute()
    {
        int x = game.fieldData.xSize;
        int y = game.fieldData.ySize;
        for (int i = 0; i < x; i++)
        {
            var column = game.GetEntitiesWithPositionX(i).OrderBy(e => e.positionY.value).ToArray();
            CheckLine(column);
        }
        
        for (int i = 0; i < y; i++)
        {
            var row = game.GetEntitiesWithPositionY(i).OrderBy(e => e.positionX.value).ToArray();
            CheckLine(row);
        }
    }

    private void CheckLine(GameEntity[] line)
    {
        for (int j = 0; j < line.Length - 2; j++)
        {
            if (line[j].isMoving || line[j].isStartedDissapear) continue;
            int strikeCount = 1;

            while (Combinable(line[j], line[j + strikeCount]))
            {
                strikeCount++;
                if (j + strikeCount == line.Length) break;
            }

            if (strikeCount > 2)
            {
                for (int k = 0; k < strikeCount; k++)
                    line[j + k].isHide = true;
                
                CheckGenerateGravity(line, strikeCount, j);

                j += strikeCount - 1;
            }
        }
    }

    private static void CheckGenerateGravity(GameEntity[] line, int strikeCount, int j)
    {
        if (strikeCount > 3)
        {
            var center = strikeCount / 2;
            line[j + center].isCreateGravityAfterDestroy = true;
        }
    }

    private bool Combinable(GameEntity e1, GameEntity e2)
    {
        return !e2.isStartedDissapear && !e2.isMoving && e1.tileColor.value == e2.tileColor.value;
    }
}