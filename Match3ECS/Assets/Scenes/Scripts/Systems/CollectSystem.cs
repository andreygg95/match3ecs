﻿using Entitas;
using UnityEngine;

public class CollectSystem : IExecuteSystem
{
    private IGroup<GameEntity> collected;
    private GameContext game;
    public CollectSystem(GameContext gameContext)
    {
        game = gameContext;
        collected = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tile, GameMatcher.Collected));
    }
    
    public void Execute()
    {
        foreach (var entity in collected.GetEntities()) 
            Collect(entity);
    }

    private void Collect(GameEntity entity)
    {
        entity.isCollected = false;

        if (entity.isGravityTile)
            game.gravityDirectionEntity.isChangeGravity = true;

        if (entity.isCreateGravityAfterDestroy)
        {
            ChangeToGravity(entity);
            return;
        }

        entity.isDestroy = true;
    }

    private void ChangeToGravity(GameEntity entity)
    {
        entity.isCreateGravityAfterDestroy = false;
        entity.isGravityTile = true;
        entity.isHide = false;
        entity.isStartedDissapear = false;
        entity.gameObjectLink.value.transform.localScale = Vector3.one;
        entity.gameObjectLink.value.transform.rotation = Quaternion.Euler(Vector3.forward * 45f);
    }
}