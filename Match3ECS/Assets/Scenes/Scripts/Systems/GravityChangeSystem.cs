﻿using Entitas;

public class GravityChangeSystem : IExecuteSystem
{
    private GameContext game;
        
    public GravityChangeSystem(GameContext gameContext)
    {
        game = gameContext;
    }

    public void Execute()
    {
        var gravityEntity = game.gravityDirectionEntity;
        if (gravityEntity.isChangeGravity)
        {
            gravityEntity.isChangeGravity = false;
            bool moveDown = game.gravityDirection.value.y < 1;
            game.ReplaceGravityDirection(game.gravityDirection.value * -1);
            int x = game.fieldData.xSize;
            int y = game.fieldData.ySize;
                
            for (int i = 0; i < x; i++)
            {
                var column = game.GetEntitiesWithPositionX(i);
                if (column.Count < y)
                {
                    var delta = y - column.Count;
                    if (!moveDown) delta *= -1;

                    foreach (GameEntity entity in column)
                    {
                        entity.ReplacePositionY(entity.positionY.value + delta);
                        entity.isMoving = true;
                    }
                }
            }
        }
    }
}