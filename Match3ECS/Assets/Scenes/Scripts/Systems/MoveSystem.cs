﻿using Entitas;
using UnityEngine;

public class MoveSystem : IExecuteSystem
{
    private GameContext game;
    private IGroup<GameEntity> moveables;
    public MoveSystem(GameContext gameContext)
    {
        game = gameContext;
        moveables = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tile, GameMatcher.WorldPosition,
            GameMatcher.Moving));
    }
    
    public void Execute()
    {
        float dt = Time.deltaTime;
        float dSpeed = dt * game.gravityValue.value;

        foreach (var tile in moveables)
        {
            if (tile.hasMoveSpeed)
                tile.moveSpeed.value += dSpeed;
            else
                tile.AddMoveSpeed(dSpeed);
            tile.worldPosition.value += tile.moveSpeed.value * dt * (Vector3)game.gravityDirection.value;
        }
    }
}