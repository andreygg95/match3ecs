﻿using Entitas;
using UnityEngine;

public class RenderSystem : IExecuteSystem
{
    private IGroup<GameEntity> entities;
    
    public RenderSystem(GameContext game)
    {
        entities = game.GetGroup(GameMatcher.AllOf(GameMatcher.Tile).NoneOf(GameMatcher.GameObjectLink));
    }

    public void Execute()
    {
        foreach (GameEntity entity in entities.GetEntities())
            AddGameObject(entity);
    }

    private void AddGameObject(GameEntity entity)
    {
        var go = new GameObject(entity.tileColor.value.ToString());
        
        go.AddComponent<MeshFilter>().sharedMesh = new Mesh
        {
            vertices =  new []{Vector3.left /2, Vector3.up/2, Vector3.right/2, Vector3.down/2 },
            triangles = new []{0,1,2,0,2,3}
        };
        
        go.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Calligraphy/Color"))
            {color = GetColor(entity.tileColor.value)};

        go.AddComponent<TapHandler>();

        var boxCollider = go.AddComponent<BoxCollider>();
        boxCollider.enabled = false;
        entity.AddCollider(boxCollider);

        entity.isColliderEnabled = false;
        
        entity.AddGameObjectLink(go);
    }

    private Color GetColor(TileData.TileColor tileColor)
    {
        switch (tileColor)
        {
            case TileData.TileColor.Blue : return Color.blue;
            case TileData.TileColor.Red : return Color.red;
            case TileData.TileColor.Green : return Color.green;
            case TileData.TileColor.Pink : return Color.magenta;
            case TileData.TileColor.Yellow : return Color.yellow;
            case TileData.TileColor.Orange : return new Color(1,.5f,0);
        }
        return Color.white;
    }
}