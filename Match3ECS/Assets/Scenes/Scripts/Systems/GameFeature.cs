﻿public class GameFeature : Feature
{
    public GameFeature(GameContext game)
    {
        Add(new InitUniques(game));
        Add(new SpawnSystem(game));
        Add(new RenderSystem(game));
        Add(new MoveSystem(game));
        Add(new StopFallingSystem(game));
        Add(new CombinationsFeature(game));
        Add(new HideSystem(game));
        Add(new CollectSystem(game));
        Add(new DestroySystem(game));
        Add(new GravityChangeSystem(game));

        Add(new ColliderSystem(game));
        Add(new TapSystem(game));
        
        Add(new SetPosition(game));
    }
}