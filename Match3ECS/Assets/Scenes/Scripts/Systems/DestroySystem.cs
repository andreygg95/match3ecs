﻿using Entitas;
using UnityEngine;

public class DestroySystem : IExecuteSystem
{
    private GameContext game;
    private IGroup<GameEntity> destroing;
    
    public DestroySystem(GameContext gameContext)
    {
        game = gameContext;
        destroing = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tile, GameMatcher.Destroy));
    }
    
    public void Execute()
    {
        foreach (var entity in destroing.GetEntities())
            Destroy(entity);
    }
    
    private void Destroy(GameEntity entity)
    {
        GameObject.Destroy(entity.gameObjectLink.value);
        var moveDown = game.gravityDirection.value.y < 1;
        var delta = moveDown ? -1 : 1;
        
        foreach (var columnEntity in game.GetEntitiesWithPositionX(entity.positionX.value))
        {
            if(columnEntity == entity)continue;
            
            if (columnEntity.positionY.value > entity.positionY.value == moveDown)
            {
                columnEntity.ReplacePositionY(columnEntity.positionY.value + delta);
                columnEntity.isMoving = true;
            }
        }

        entity.Destroy();
    }
}