﻿using Entitas;
using UnityEngine;

public class StopFallingSystem : IExecuteSystem
{
    private GameContext game;
    private IGroup<GameEntity> moveables;
    public StopFallingSystem(GameContext gameContext)
    {
        game = gameContext;
        moveables = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tile, GameMatcher.MoveSpeed));
    }
    
    public void Execute()
    {
        bool moveDown = game.gravityDirection.value.y < 0;

        foreach (var entity in moveables.GetEntities())
        {
            int y = entity.positionY.value;
            float yWorld = entity.worldPosition.value.y;
            if (moveDown == y > yWorld)
            {
                entity.ReplaceWorldPosition(new Vector3(entity.positionX.value, y));
                entity.isMoving = false;
                entity.RemoveMoveSpeed();
            }
        }
    }
}