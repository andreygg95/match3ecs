﻿using Entitas;

public class SetPosition : IExecuteSystem
{
    private IGroup<GameEntity> entityes;
    public SetPosition(GameContext gameContext)
    {
        entityes = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tile, GameMatcher.WorldPosition,
            GameMatcher.GameObjectLink));
    }
    
    public void Execute()
    {
        foreach (var tile in entityes)
            tile.gameObjectLink.value.transform.position = tile.worldPosition.value;
    }
}